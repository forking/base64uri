import { Base64 } from 'js-base64'

function encode (params) {
  return Base64.encodeURI(JSON.stringify(params))
}

function decode (params) {
  let paramsRaw = Base64.decode(params)
  try {
    paramsRaw = JSON.parse(paramsRaw)
  } catch (e) {
    if (e) {}
  }
  return paramsRaw
}

export default {
  encode: encode,
  decode: decode
}
