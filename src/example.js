import Base64URI from './'

let strIn = {
  a: 'A',
  b: ['b', 'B'],
  c: '?a=A&b=s p a c e+-'
}
let strEn = Base64URI.encode(strIn)
let strDe = Base64URI.decode(strEn)

console.log('raw:', strIn)
console.log('encode:', strEn)
console.log('decode:', strDe)
