# Base64URI

Base64 for URI.

## Installation

```
yarn add bitbucket:forking/base64uri
```

## Usage

```js
import Base64URI from 'base64uri'
```
