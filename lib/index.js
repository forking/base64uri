'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jsBase = require('js-base64');

function encode(params) {
  return _jsBase.Base64.encodeURI(JSON.stringify(params));
}

function decode(params) {
  var paramsRaw = _jsBase.Base64.decode(params);
  try {
    paramsRaw = JSON.parse(paramsRaw);
  } catch (e) {
    if (e) {}
  }
  return paramsRaw;
}

exports.default = {
  encode: encode,
  decode: decode
};