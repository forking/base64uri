'use strict';

var _ = require('./');

var _2 = _interopRequireDefault(_);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var strIn = {
  a: 'A',
  b: ['b', 'B'],
  c: '?a=A&b=s p a c e+-'
};
var strEn = _2.default.encode(strIn);
var strDe = _2.default.decode(strEn);

console.log('raw:', strIn);
console.log('encode:', strEn);
console.log('decode:', strDe);